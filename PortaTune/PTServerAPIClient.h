//
//  PTServerAPIClient.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MKNetworkKit.h"
@interface PTServerAPIClient : NSObject 
+ (PTServerAPIClient*)sharedClient;
+ (NSString*)getCurrentEndpoint;
+ (BOOL) isWorkingLocally;
+ (NSString *)currentWifiSSID;
+ (NSString *) makeLinkForAudiofile: (NSString*)audioID;
- (void) sendRequest:(NSString*)request withParameters:(NSDictionary*)params andCallback:(void(^)(id result, NSError* error))callback;

@end
