//
//  PTDigitallyImportedTuner.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 06/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MKNetworkKit.h"
#import "PTPlaylist.h"
#import "PTRadioStation.h"

#define DI_TIER @"public2"
@interface PTDigitallyImportedTuner : NSObject
+ (void) getStationListForCallback:(void (^)(PTPlaylist *))success;
+ (void) resolveStationURLForStation:(PTRadioStation*)station callback:(void (^)(PTRadioStation *))success;
@end
