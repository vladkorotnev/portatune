//
//  PTVKComm.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 06/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PTTrack.h"
@interface PTVKComm : NSObject
+ (void) resolveTrack:(PTTrack*)station callback:(void (^)(PTTrack *))success;
@end
