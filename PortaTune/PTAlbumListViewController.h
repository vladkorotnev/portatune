//
//  PTAlbumListViewController.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 06/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTPlaylistListViewController.h"
#import "PTUserConnection.h"
#import "PTMainViewController.h"
@interface PTAlbumListViewController : PTPlaylistListViewController

@end
