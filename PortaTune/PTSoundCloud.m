//
//  PTSoundCloud.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 05/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTSoundCloud.h"
#import "MKNetworkKit.h"
@implementation PTSoundCloud
+(NSURL*)getTrackURLForPTTID:(NSString *)tid {
    NSString* t = [tid substringFromIndex:2];
    NSURL* requrl = [NSURL URLWithString: [NSString stringWithFormat:@"http://api.soundcloud.com/tracks/%@/stream?consumer_key=5a25404a12be3b9d52c91ac5d1c78bd2",t]];
     return requrl;
}
@end
