//
//  BTAppDelegate.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//


///  [[MPVolumeView alloc] initWithFrame:<frame> style:4];  ////

#import "BTAppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import "PTPlayer.h"
#import "MKNetworkKit.h"
@implementation BTAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDir    = [myPathList  objectAtIndex:0];
    for (NSString*paths in [[NSFileManager defaultManager]contentsOfDirectoryAtPath:[cacheDir stringByAppendingPathComponent:@"incomplete"] error:nil]) {
        [[NSFileManager defaultManager]removeItemAtPath:paths error:nil];
    }

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [[NSUserDefaults standardUserDefaults]synchronize];

}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}
#pragma mark Remote Control Events
/* The iPod controls will send these events when the app is in the background */
- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    NSLog(@"Got remote event");
	switch (event.subtype) {
		case UIEventSubtypeRemoteControlTogglePlayPause:
			if([[PTPlayer sharedInstance] isPlaying]) [[PTPlayer sharedInstance] pause];
            else [[PTPlayer sharedInstance] play];
			break;
		case UIEventSubtypeRemoteControlPlay:
			[[PTPlayer sharedInstance] play];
			break;
		case UIEventSubtypeRemoteControlPause:
			[[PTPlayer sharedInstance] pause];
			break;
		case UIEventSubtypeRemoteControlStop:
			[[PTPlayer sharedInstance] pause];
			break;
        case UIEventSubtypeRemoteControlNextTrack:
            [[PTPlayer sharedInstance] next];
            break;
        case UIEventSubtypeRemoteControlPreviousTrack:
            [[PTPlayer sharedInstance] previous];
            break;
		default:
			break;
	}
}

- (void) startRemote{
    [[UIApplication sharedApplication]beginReceivingRemoteControlEvents ];
    [self becomeFirstResponder];
}
- (void) endRemote {
    [[UIApplication sharedApplication]endReceivingRemoteControlEvents ];
}

@end
