//
//  PTAlbumListViewController.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 06/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTAlbumListViewController.h"

@interface PTAlbumListViewController ()
{
    NSArray*allLists;
}
@end

@implementation PTAlbumListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)npbtn:(id)sender {
    [(PTMainViewController*)self.tabBarController nowPlaying];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}
- (NSArray*)lists { return allLists; }
- (void) refresh {
    [self.refreshControl beginRefreshing];
    PTUserConnection*u = [PTUserConnection sharedUserConnection];
    if ([u isLoggedIn]) {
        [u getAlbumsForCallback:^(NSArray *playlists) {
            allLists = [playlists copy];
            [self.tableView reloadData];
            [self.refreshControl endRefreshing];
        }];
    } else {
        
    }
}
bool oneTimeRefresh;
- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!oneTimeRefresh) {
        oneTimeRefresh = true;
        [self refresh];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh) name:@"PortaTuneDidLogin" object:nil];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
