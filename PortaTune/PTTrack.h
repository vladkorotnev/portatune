//
//  PTTrack.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PTPlaylist.h"
@interface PTTrack : NSObject
@property NSString* name;
@property NSString* artist;
@property NSString* album;
@property NSString* identifier;
@property NSString* totaltime;
@property NSString* extradata;
@property PTPlaylist* containingPlaylist;
@property bool isNowPlaying;
- (BOOL) isRadioStation;
- (NSString*) userReadableRepresentation;
@end
