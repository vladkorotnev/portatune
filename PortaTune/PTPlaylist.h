//
//  PTPlaylist.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PTPlaylist : NSObject
@property NSInteger identifier;
@property NSString *name;
@property NSArray *contents;
@property NSString* artist;
@property bool isAlbum;
@end
